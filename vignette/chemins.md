This tool tries to address the issue of the segmentation of genomic
profile from next generation sequencing data (NGS). The proposed
approach is to slide by a fix window trought the data and compare the
first half of the window with the second half, to compute the likelihood
of a break point happening.

The comparison is performed by the Kolmogorov-Smirnov test (default),
the mean of the difference of the first and second half of the window
(less reliable for small changes, but very fast), or a combination of
the two approaches.

## Simulate date

Simulate normal and tumor data, average 50 reads per position

### Normal

The normal sample have ideally a constant number of reads per each
position, and the two alleles have a ration of 0.5 each

``` r
allele_n_a <- rbinom(1000, 50, 0.5)
allele_n_b <- rbinom(1000, 50, 0.5)

normal_sim <- allele_n_a + allele_n_b
```

### Tumor sample

The tumor sample may have an irregular coverage profile accompanied by
allelic imbalance

``` r
allele_t_a <- c(rbinom(200, 50, 0.5),
    rbinom(100, 10, 0.8),
    rbinom(300, 40, 0.6),
    rbinom(200, 50, 0.5),
    rbinom(200, 80, 0.7))

allele_t_b <- c(rbinom(200, 50, 0.5),
    rbinom(100, 10, 0.2),
    rbinom(300, 40, 0.4),
    rbinom(200, 50, 0.5),
    rbinom(200, 80, 0.3))

tumor_sim <- allele_t_a + allele_t_b
```

### Ratio and B-allele frequency

The depth ratio and the allele frequency profiles are used to
investigate DNA locations with coverage and alleles change points

``` r
ratio_sim <- tumor_sim / normal_sim
Bf_sim <- allele_t_b / tumor_sim

Bf_sim[Bf_sim > 0.5] <- 1 - Bf_sim[Bf_sim > 0.5]
is_het <- rep(TRUE, length(Bf_sim))
```

``` r
plot(ratio_sim, xlab = "position", ylab = "ratio", las = 1)
```

![picture](chemins_files/figure-gfm/plot_profile1-1.png)

``` r
plot(Bf_sim, xlab = "position", ylab = "BAF", las = 1)
```

![picture](chemins_files/figure-gfm/plot_profile1-2.png)

## Chemins

The naive approach of *chemins* is to use a sliding window throughout
the data, and compare the first half of the window to the second half.

### Sliding window diffs

``` r
library(chemins)
ratio_diffs <- slide_matrix(ratio_sim, w = 100)
```

    ## Warning in if (method == "kstest") {: the condition has length > 1 and only
    ## the first element will be used

``` r
Bf_diffs <- slide_matrix(Bf_sim, w = 100)
```

    ## Warning in if (method == "kstest") {: the condition has length > 1 and only
    ## the first element will be used

### Difference in methods

``` r
ratio_diffs_1 <- slide_matrix(ratio_sim, w = 100, method = "kstest")
ratio_diffs_2 <- slide_matrix(ratio_sim, w = 100, method = "meandiff")
ratio_diffs_3 <- slide_matrix(ratio_sim, w = 100, method = "both")
```

``` r
plot(x = ratio_diffs_1$x, y = ratio_diffs_1$y, las = 1,
     ylab = "diffs", xlab = "position", type = "l",
     ylim = c(0, 1))
lines(x = ratio_diffs_2$x, y = ratio_diffs_2$y, col = "red")
lines(x = ratio_diffs_3$x, y = ratio_diffs_3$y, col = "green")
legend("top", c("ks.test", "meandiff", "both"), fill = c("black", "red", "green"),
       bty = "n", title = "diffs")
```

![picture](chemins_files/figure-gfm/plot_methods-1.png)

``` r
plot(x = ratio_diffs$x, y = ratio_diffs$y, las = 1,
     ylab = "diffs", xlab = "position", type = "l",
     ylim = c(0, 1))
lines(x = Bf_diffs$x, y = Bf_diffs$y, col = "red")
lines(x = ratio_diffs$x, y = (Bf_diffs$y + ratio_diffs$y) / 2,
      col = "green")
legend("top", c("ratio", "BAF", "both"), fill = c("black", "red", "green"),
       bty = "n", title = "diffs")
```

![picture](chemins_files/figure-gfm/plot_diffs-1.png)

### Find local max differences

``` r
peaks_ratio <- get_peaks(x = ratio_diffs$y, position = ratio_diffs$x,
                          w = 100)
peaks_baf <- get_peaks(x = Bf_diffs$y, position = Bf_diffs$x,
                        w = 100)
peaks_both <- get_peaks(x = (Bf_diffs$y + ratio_diffs$y) / 2,
                         position = ratio_diffs$x, w = 100)
```

Example of the resulting object, peaks detected from the depth ratio
(left), allele frequency (center), and from both (right)

| x | x | x |
|---|---|---|
|201| 83|197|
|304|197|304|
|418|304|420|
|480|396|481|
|593|482|592|
|807|590|697|
|911|702|797|
|   |794|913|
|   |926|   | 

Let’s test the guessed breakpoints:

``` r
plot(ratio_sim, xlab = "position", ylab = "ratio", las = 1)
abline(v = peaks_ratio)
abline(v = peaks_baf, col = "red")
abline(v = peaks_both, col = "green")
```

![picture](chemins_files/figure-gfm/plot_profile2-1.png)

``` r
plot(Bf_sim, xlab = "position", ylab = "BAF", las = 1)
abline(v = peaks_ratio)
abline(v = peaks_baf, col = "red")
abline(v = peaks_both, col = "green")
```

![picture](chemins_files/figure-gfm/plot_profile2-2.png)

## Try chemins with sequenza test data

Assuming *sequenza* is already installed in the system

### Read chromosome 1

``` r
library(sequenza)
data.file <-  system.file("extdata", "example.seqz.txt.gz", package = "sequenza")
chr1 <- read.seqz(data.file, chr_name="1")
chr1 <- chr1[chr1$zygosity.normal == "het", ]
```

### Use chemins on the ratio and B-allele

Without normalizing the
data….

``` r
ratio_diffs <- slide_matrix(chr1$depth.ratio, w = 100, position = chr1$position)
```

    ## Warning in if (method == "kstest") {: the condition has length > 1 and only
    ## the first element will be used

``` r
Bf_diffs <- slide_matrix(chr1$Bf, w = 100, position = chr1$position)
```

    ## Warning in if (method == "kstest") {: the condition has length > 1 and only
    ## the first element will be used

### Find peaks, and breakpoints

``` r
peaks_both <- get_peaks(x = (ratio_diffs$y + Bf_diffs$y) / 2,
                         position = ratio_diffs$x, w = 100)
```

Let’s ignore the centromere information and computer the segment valuyes
taking only the estimated breakpoints:

``` r
coords <- peaks_both
pos_start <- c(chr1$position[1], coords)
pos_end <- c(coords, chr1$position[length(chr1$position)])
breaks <- data.frame(chrom = "1", start.pos = pos_start, end.pos = pos_end - 1)
```

### Use the breakpoints with sequenza

``` r
test <- sequenza.extract(data.file, chromosome.list = c(1), breaks = breaks)
```

    ## Collecting GC information
    ## . done
    ## 
    ## Processing 1:
    ##    63 variant calls.
    ##    35 copy-number segments.
    ##    4389 heterozygous positions.
    ##    1428 homozygous positions.

``` r
chromosome.view(mut.tab = test$mutations[[1]], baf.windows = test$BAF[[1]],
                ratio.windows = test$ratio[[1]],  min.N.ratio = 1,
                segments = test$segments[[1]],
                main = test$chromosomes[1],
                cellularity = 0.89, ploidy = 1.9,
                avg.depth.ratio = 1)
```

![picture](chemins_files/figure-gfm/chromosome_view-1.png)
